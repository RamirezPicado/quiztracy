using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clase_4.Models;

namespace clase_4.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]

    public class RegistersController : ControllerBase
    {
        private readonly DataBaseContext _context;
       

        public RegistersController(DataBaseContext context)
        {
            _context = context;
           
        }

        // GET: api/Vehicles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Register>>> GetRegister()
        {
            return await _context.Registers.ToListAsync();
        }

        // GET: api/Vehicles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Register>> GetRegister(long id)
        {
            var register= await _context.Registers.FindAsync(id);

            if (register== null)
            {
                return NotFound();
            }

            return register;
        }


        //POST: api/Registers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Register>> PostRegister(Register register)
        {
            _context.Registers.Add(register);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRegister", new { id = register.Id},register);
        }

    }
}
