import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-registers',
  templateUrl: './registers.component.html',
  styleUrls: ['./registers.component.css']
})
export class RegistersComponent {

  public registers: Register[];
  public register: Register;
  
  //public showForm = false;

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) { 
  }
  
  save() {
    this.http.post(this.baseUrl + 'api/registers', this.register).subscribe((response) => {
      console.log(response);
    }, error => console.error(error));
  }
  
}

interface Register{
  
  identification:string;
  name:string;
  email:string;
  telephone:string;
  password:string;


}
